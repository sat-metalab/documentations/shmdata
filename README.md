# Shmdata docs : a first website prototype

## License

This documentation is licensed under a [Creative Commons Attribution Non-Commercial ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-nc-sa/4.0/)

All the source code (including examples and excerpt from the software) is licensed under the [GNU LesserPublic License version 3 (LGPLv3)](https://www.gnu.org/licenses/lgpl-3.0.en.html)

## Code of Conduct

By participating in this project, you agree to abide by the [Code of Conduct](CODE_OF_CONDUCT.md). We expect all contributors to follow the [Code of Conduct](CODE_OF_CONDUCT.md) and to treat fellow humans with respect. It must be followed in all your interactions with the project.

## Website URLs

The landing page is accessible here: https://sat-metalab.gitlab.io/documentations/shmdata/en/

The documentation is accessible here: https://sat-metalab.gitlab.io/documentations/shmdata/en/contents.html

## Overview

This repo is the [Shmdata](https://www.gitlab.com/sat-metalab/shmdata) documentation repo. This project was started in early 2021 as a first step towards user-facing documentation of the Shmdata project.

You will find the current roadmap of the documentation project in the `ROADMAP.md` file.

## Building the docs

To be build with Sphinx.

After cloning this repo with `git clone`, you will be able to build locally the documentation with the following instructions:

    sphinx-build -b html -D language=en source build/html/en
    sphinx-build -b html -D language=fr source build/html/fr
    
You can display the resulting HTML pages in your browser by running a local server and then, navigating to the `build/html/en` folder or to the `build/html/fr` folder.

To run a local server with Python:

    python3 -m http.server
    
If you want to learn more about Sphinx, you may consult this [Sphinx workshop](https://gitlab.com/sat-metalab/workshops/sphinx-workshop) that was given at the Metalab.

## Contributing to the docs

Please [open an issue on GitLab](https://gitlab.com/sat-metalab/documentations/shmdata/-/issues/new) if you have an idea or a suggestion for how to improve the docs, or if you found a bug in the documentation.

