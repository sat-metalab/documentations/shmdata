Installation
============

User installation
-----------------

To install shmdata on Ubuntu 20.04 through the `Metalab's PPA <https://launchpad.net/~sat-metalab/+archive/ubuntu/metalab/>`__, open a Terminal window and run the following commands:

.. code-block:: bash

        sudo add-apt-repository ppa:sat-metalab/metalab
        sudo apt-get update
        sudo apt install libshmdata

The project should now be up and running. You can test your installation of shmdata by following the :ref:`first tutorial`.

Developer installation
----------------------

If you wish to install shmdata from sources, the up-do-date instructions are available on Gitlab: `Link install-from-sources <https://gitlab.com/sat-metalab/shmdata/-/blob/master/doc/install-from-sources.md>`__.

Before contributing to the project, please read the `CONTRIBUTING guidelines <https://gitlab.com/sat-metalab/shmdata/-/blob/master/CONTRIBUTING.md>`__.
