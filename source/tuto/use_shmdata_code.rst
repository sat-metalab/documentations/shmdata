Use shmdata in your code
~~~~~~~~~~~~~~~~~~~~~~~~

In previous section, GStreamer is used in order to illustrate a
transmission, but you can also include shmdata in you project with
specific languages, using native shmdata API. Here follows some examples
in various languages:

-  `C++ <https://gitlab.com/sat-metalab/shmdata/-/tree/master/tests/check-writer-follower.cpp>`__
-  `C <https://gitlab.com/sat-metalab/shmdata/-/tree/master/tests/check-c-wrapper.cpp>`__
-  `Python3 <https://gitlab.com/sat-metalab/shmdata/-/tree/master/wrappers/python/example.py>`__
-  `GStreamer writer <https://gitlab.com/sat-metalab/shmdata/-/tree/master/gst/check-shmdatasink.c>`__
-  `GStreamer follower <https://gitlab.com/sat-metalab/shmdata/-/tree/master/gst/check-shmdatasrc.c>`__

Note that in order to develop with shmdata C or C++ API, you may want to
install development files:

.. code-block:: bash

   sudo apt install libshmdata-dev

Then C and C++ using shmdata can be compiled with help from pkgconfig,
as follows:

.. code-block:: bash

   # C code:
   gcc -o check-c-wrapper $(pkg-config --cflags shmdata-1.3) ./check-c-wrapper.cpp $(pkg-config --libs shmdata-1.3)

