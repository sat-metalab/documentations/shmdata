shmdata
=======

.. warning::

   We are in the process of improving the documentation. You can `test the upcoming documentation here <contents.html>`__, and you are very welcome to signal issues with the documentation `on the issue tracker <https://gitlab.com/sat-metalab/documentations/shmdata/-/issues>`__

.. container:: index-locales

   `En <../en/index.html>`__ -=- `Fr <../fr/index.html>`__

.. container:: index-intro

   welcome to the shmdata documentation website
  
.. container:: index-menu

   `-` `current documentation <https://gitlab.com/sat-metalab/shmdata/-/blob/master/README.md>`__ - `gitlab <https://gitlab.com/sat-metalab/shmdata>`__- `about us <https://sat.qc.ca/fr/recherche/metalab>`__ - `get in touch <https://gitlab.com/sat-metalab/shmdata/-/issues>`__ -
  

.. container:: index-section

    what is shmdata?
  
A library to share streams of framed data between processes via shared memory. Without using a server and without copying, shmdata allows the streaming of data between colocated programs (programs on the same computer).

It supports any kind of data stream:  it has been used with multichannel audio, video frames, 3D models, OSC messages, and various others types of data. Shmdata is server-less, but requires applications to link data streams using socket path (e.g. "/tmp/my-shmdata-stream"). Shmdata is very fast and allows processes to access data streams without the need for extra copies.

.. container:: index-section

   how does it works?
  
The communication paradigm is 1 to many, i.e., one writer is making available data frames to several followers. Followers and writers can hot connect & disconnect. Shmdata transmission supports buffer resizing. Each shmdata has a type specified with a string description, itself published by the shmdata writer at each reconnection. The type is specified as a user-defined string. Although left to the user, we encourage type specification to follow GStreamer 1.0 caps specification format, for instance "audio/x-raw,format=S16LE,channels=2,layout=interleaved".
  
Note the existence of `NDI2shmdata <https://gitlab.com/sat-metalab/ndi2shmdata>`__ that converts shmdata to NewTek's NDI, and vice versa.
   
.. container:: index-section

   where can I get shmdata?

Start by `installing shmdata for Ubuntu <./installation/contents.html>`__.
  
.. container:: index-section

   table of contents
   
* how to `install shmdata <./installation/contents.html>`__
* `tutorials <./tuto/contents.html>`__ to grow your skills
  
.. container:: index-section

   please show me the code!
   
For more information visit `the code repository <https://gitlab.com/sat-metalab/shmdata>`__.
  
.. container:: index-section

   sponsors

This project is made possible thanks to the `Society for Arts and Technologies <http://www.sat.qc.ca>`__ (also known as SAT).
