*Contributing is about the necessary steps to build the -dev version of the project.*

--------------------------------------------------------------------------------

# Contributing

Our project follows [Code of Conduct link]. It applies to all contributions.

## Installing the -dev branch of the project

If you wish to install shmdata from sources, the up-do-date instructions are available on Gitlab: [install-from-sources](https://gitlab.com/sat-metalab/shmdata/-/blob/master/doc/install-from-sources.md).

## Building the project locally

## Making a change to the project

## Testing the change locally

## Pushing the branch to Gitlab

## Making a MR

## Following up on a MR

## what to expect next

## Coding style

We use Google C++ Style Guide, as described [here](https://google.github.io/styleguide/cppguide.html), with two exceptions:
* A function call’s arguments will either be all on the same line or will have one line each. 
* A function declaration’s or function definition’s parameters will either all be on the same line or will have one line each.

For Python code, we use PEP8 style guide with maximum line length set to 120.

## Adding your name to contributors.md



