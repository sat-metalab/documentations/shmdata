Welcome to shmdata documentation
================================

.. toctree::
    :maxdepth: 2
    
    installation/contents
    tuto/contents
    reference/contents
    
    Back to main page <index>


Start by installing shmdata, and then check your installation by following the Getting Started tutorial.
